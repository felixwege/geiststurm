from lib.motor import Motor


class Arm(object):
    def __init__(self, vm):
        self._rotateMotor = Motor(vm, "C")
        self._pitchMotor = Motor(vm, "D")
        self._grabMotor = Motor(vm, "A")

    def set_speed(self, percentage: int):
        self._rotateMotor.set_speed(percentage)
        self._pitchMotor.set_speed(percentage)
        self._grabMotor.set_speed(percentage)

    async def open_grabber(self):
        await self._grabMotor.run_to_degrees(0)

    async def close_grabber(self):
        await self._grabMotor.run_to_degrees(-100)

    async def pitch_up(self):
        await self._pitchMotor.run_to_degrees(0)

    async def pitch_down(self):
        await self._pitchMotor.run_to_degrees(170)

    async def rotate_by(self, degrees):
        await self._rotateMotor.run_by_degrees_relative(degrees)
