import hub
import math
from util.scratch import clamp, percent_to_int
from util.sensors import is_type, get_sensor_value


class DistanceSensor(object):
    def __init__(self, vm, port: str):
        self._vm = vm
        self._motor = vm.system.motors.on_port(port)
        self._port = port

    def read_distance(self):
        sensor_value = get_sensor_value(
            self._port,
            0,  # long-range distance measurement mode
            200,  # default value if unknown
            (62,),
        )  # use for Technic distance sensor
        if sensor_value is None:
            sensor_value = 200  # max range is 200 centimeters = 2 meters
        return sensor_value

    def set_lights(self, ul: bool, ur: bool, ll: bool, lr: bool):
        # Ultrasonic light up
        port = getattr(hub.port, self._port, None)
        if getattr(port, "device", None) and is_type(
            self._port, 62
        ):  # for ultrasonic sensor
            # This could be simplified by operating on lists rather than strings.
            data = "".join(
                [
                    chr(percent_to_int(math.floor(clamp(p, 0, 100) + 0.5), 87))
                    for p in [
                        100 if ul else 0,
                        100 if ur else 0,
                        100 if ll else 0,
                        100 if lr else 0,
                    ]
                ]
            )  # "UL UR LL LR" brightness percentage
            # If your data is simply a list then remove the encoding parameter.
            port.device.mode(5, bytes(data, "utf-8"))
