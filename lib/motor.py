class Motor(object):
    def __init__(self, vm, port: str):
        self._vm = vm
        self._motor = vm.system.motors.on_port(port)
        self._port = port

    def run_at_speed(self):
        (acceleration, deceleration) = self._vm.store.motor_acceleration("A")
        self._vm.system.motors.on_port(self._port).run_at_speed(
            -self._vm.store.motor_speed(self._port),
            stall=self._vm.store.motor_stall(self._port),
            acceleration=acceleration,
            deceleration=deceleration,
        )

    async def run_to_degrees(self, degrees: int):
        (acceleration, deceleration) = self._vm.store.motor_acceleration(self._port)
        self._vm.store.motor_last_status(
            self._port,
            await self._vm.system.motors.on_port(self._port).run_to_position_async(
                degrees,
                abs(self._vm.store.motor_speed(self._port)),
                "shortest",  # or: "clockwise", "counterclockwise"
                stall=self._vm.store.motor_stall(self._port),
                stop=self._vm.store.motor_stop(self._port),
                acceleration=acceleration,
                deceleration=deceleration,
            ),
        )

    async def run_for_time(self, ms: int, direction: str = "clockwise"):
        (acceleration, deceleration) = self._vm.store.motor_acceleration(self._port)
        current_speed = self._vm.store.motor_speed(self._port)
        sign = 1 if direction == "clockwise" else -1
        self._vm.store.motor_last_status(
            self._port,
            await self._motor.run_for_time_async(
                ms,
                sign * current_speed,  # positive for clockwise
                stall=self._vm.store.motor_stall(self._port),
                stop=self._vm.store.motor_stop(self._port),
                acceleration=acceleration,
                deceleration=deceleration,
            ),
        )

    async def run_by_degrees_relative(self, degrees: int, speed_percent: int | None = None):
        (acceleration, deceleration) = self._vm.store.motor_acceleration(self._port)
        speed = (
            self._vm.store.motor_speed(self._port)
            if speed_percent is None
            else speed_percent
        )
        self._vm.store.motor_last_status(
            self._port,
            await self._vm.system.motors.on_port(self._port).run_for_degrees_async(
                degrees,  # degrees, 1 rotation = 360 degrees
                speed,  # percent, positive for clockwise
                stall=self._vm.store.motor_stall(self._port),
                stop=self._vm.store.motor_stop(self._port),
                acceleration=acceleration,
                deceleration=deceleration,
            ),
        )

    def stop(self):
        self._motor.stop(self._vm.store.motor_stop(self._port))

    def set_speed(self, percentage: int):
        self._vm.store.motor_speed(self._port, percentage)
