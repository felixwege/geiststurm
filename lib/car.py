from lib.motor import Motor


class Car(object):
    def __init__(self, vm):
        self._driveMotor = Motor(vm, "A")
        self._steeringMotor = Motor(vm, "B")

    async def drive(self, ms: int, direction: str = "forward"):
        motorDirection = "clockwise" if direction == "forward" else "counter-clockwise"
        await self._driveMotor.run_for_time(ms, motorDirection)

    async def set_steering_angle(self, degrees: int):
        await self._steeringMotor.run_to_degrees(degrees)

    def set_drive_speed(self, percentage: int):
        self._driveMotor.set_speed(percentage)
