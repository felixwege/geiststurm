class Display(object):
    def __init__(self, vm):
        self._vm = vm

    async def write(self, text: str):
        await self._vm.system.display.write_async(text) 