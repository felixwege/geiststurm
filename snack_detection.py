class Snackment:
    def __init__(self, start, end, n_measurements):
        self.start = start
        self.end = end
        self.center = (start + end) / 2
        self.n_measurements = n_measurements

    def __repr__(self):
        return f"start: {self.start}, end: {self.end}, center: {self.center}, n_measurements: {self.n_measurements}"


def detect_snacks(measurements):
    snackments = []
    start = None
    n_measurements = 0
    threshold = 20

    for measurement in measurements:
        angle, distance = measurement

        if distance <= threshold:
            n_measurements += 1
            if start is None:
                start = angle

        if start is not None and distance > threshold:
            snackments.append(
                Snackment(start=start, end=angle, n_measurements=n_measurements)
            )
            start = None
            n_measurements = 0

    snackments = [s for s in snackments if s.n_measurements > 1]
    return snackments


def smooth_measurements(measurements):
    smoothed = [
        (
            measurements[0][0],
            (measurements[-1][1] + measurements[0][1] + measurements[1][1]) / 3,
        )
    ]

    for i in range(1, len(measurements) - 2):
        smoothed.append(
            (
                measurements[i][0],
                (measurements[i - 1][1] + measurements[i][1] + measurements[i + 1][1])
                / 3,
            )
        )

    smoothed.append(
        (
            measurements[-1][0],
            (measurements[-2][1] + measurements[-1][1] + measurements[0][1]) / 3,
        )
    )

    return smoothed


measurements = [
    (0, 200),
    (5, 170),
    (10, 160),
    (15, 150),
    (20, 200),
    (25, 200),
    (30, 200),
    (35, 200),
    (40, 200),
    (45, 200),
    (50, 20),
    (55, 16),
    (60, 10),
    (65, 17),
    (70, 50),
    (75, 200),
    (80, 200),
    (85, 200),
    (90, 200),
    (95, 60),
    (100, 10),
    (105, 20),
    (110, 80),
    (115, 200),
    (120, 200),
]
snacks = detect_snacks(smooth_measurements(measurements))
print(snacks)
