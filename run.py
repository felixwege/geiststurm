from lib.arm import Arm


async def main(vm):
    from time import sleep
    arm = Arm(vm)
    arm.set_speed(100)
    while True:
        await arm.pitch_down()
        await arm.close_grabber()
        await arm.pitch_up()
        await arm.rotate_by(300)
        await arm.pitch_down()
        await arm.open_grabber()

    # sensor = DistanceSensor(vm, "E")
    # while True:
    #     sleep(0.5)
    #     value = sensor.read_distance()
    #     if value < 10:
    #         sensor.set_lights(False, False, False, False)
    #     elif value < 20:
    #         sensor.set_lights(True, False, False, False)
    #     elif value < 30:
    #         sensor.set_lights(True, True, False, False)
    #     elif value < 40:
    #         sensor.set_lights(True, True, True, False)
    #     else:
    #         sensor.set_lights(True, True, True, True)
