from protocol.ujsonrpc import json_rpc
import runtime
import sys
import system

{{libs}}

async def main(vm, stack):
{{program}}

def setup(rpc, system, stop):
    vm = runtime.VirtualMachine(rpc, system, stop, "car")
    vm.register_on_start("main_on_start", main)
    return vm

vm = setup(None, system.system, sys.exit)
if "program_selector" not in json_rpc.methods:
    json_rpc.add_method("program_selector", json_rpc.methods["program_terminate"])
def terminate(params, id):
    vm.shutdown()
    json_rpc.methods["program_selector"](params, id)
json_rpc.add_method("program_terminate", terminate)
vm.start()
