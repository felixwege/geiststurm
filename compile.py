from jinja2 import Environment, FileSystemLoader

environment = Environment(loader=FileSystemLoader("templates/"))
template = environment.get_template("robot_template.py")

libs = ""
with open("lib/motor.py", "r") as f:
    motor = f.readlines()[:]
    motor = "".join(motor)
    libs += f"{motor}\n"
with open("lib/car.py", "r") as f:
    car = f.readlines()[1:]
    car = "".join(car)
    libs += f"{car}\n"
with open("lib/display.py", "r") as f:
    display = f.readlines()[:]
    display = "".join(display)
    libs += f"{display}\n"
with open("lib/sensors.py", "r") as f:
    sensors = f.readlines()[:]
    sensors = "".join(sensors)
    libs += f"{sensors}\n"
with open("lib/arm.py", "r") as f:
    arm = f.readlines()[1:]
    arm = "".join(arm)
    libs += f"{arm}\n"

with open("run.py", "r") as f:
    lines = f.readlines()[4:]

program = "".join(lines)
output = template.render(libs=libs, program=program)
print(output)

with open("robot.py", "w") as f:
    f.write(output)
